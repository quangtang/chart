import sys
import numpy
import csv
import random
import time
import math
import socket
import pylab
from  matplotlib.colors import LogNorm
import matplotlib.cm
import matplotlib.colorbar

from pylab import *

def resizeMat(M,pt,ol):
	dp = int(pt*(1-ol))
	N = []
	for i in xrange(0,len(M),dp):
		T = M[i:min(i+pt,len(M))]
		if len(T) == pt:
			N.append(T)
	return N

def fftMat(M):
	N = []
	for i in xrange(len(M)):
		fd = numpy.fft.fft(M[i])
		fr = fd.real
		fi = fd.imag
		fd2 = fr**2+fi**2
		fds = numpy.array(map(numpy.log10,fd2[1:(len(M[0])/2)]))
		idx = numpy.nonzero(numpy.isinf(fds))[0]
		fds[idx] = 0
		N.append(fds)
	return N

if __name__ == '__main__':

	plot_flg = 1			# 1:show, 0:hide

	timeFlg = 1
	titleFlg = 1

	gname = None
	graphname = "fft.png"
	fig_dpi = 300
	gx = 8
	gy = 6
	graph_flg = False

	fftNum = 512
	OverLap = 0.75

	mode = 1

	px = 0

	log_flg = 0

	xmx = None
	ymx = None
	zmn = None
	outname = None
	zmn = None
	zmx = None

	for i in range(1,len(sys.argv)):
		if len(sys.argv[i]) > 0 and sys.argv[i][0] == '-' and len(sys.argv) > i:
			if sys.argv[i] == '-in':
				fname = sys.argv[i+1]
			elif sys.argv[i] == '-lTM':
				timeFlg = int(sys.argv[i+1])
			elif sys.argv[i] == '-lTL':
				titleFlg = int(sys.argv[i+1])
			elif sys.argv[i] == '-plot':	# plot flag
				plot_flg = int(sys.argv[i+1])
			elif sys.argv[i] == '-out':
				outname = sys.argv[i+1]
			elif sys.argv[i] == '-mode':    # 1:fft->plot(default), 0:plot only
				mode = int(sys.argv[i+1])
			elif sys.argv[i] == '-px':
				px = int(sys.argv[i+1])
			elif sys.argv[i] == '-fn':
				fftNum = int(sys.argv[i+1])
			elif sys.argv[i] == '-ol':
				OverLap = float(sys.argv[i+1])
			elif sys.argv[i] == '-gx':
				gx = int(sys.argv[i+1])
			elif sys.argv[i] == '-gy':
				gy = int(sys.argv[i+1])
			elif sys.argv[i] == '-graph':
				graph_flg = int(sys.argv[i+1])
			elif sys.argv[i] == '-fG':
				graphname = sys.argv[i+1]
			elif sys.argv[i] == '-log':
				log_flg = int(sys.argv[i+1])
			elif sys.argv[i] == '-xmx':
				xmx = int(sys.argv[i+1])
			elif sys.argv[i] == '-ymx':
				ymx = int(sys.argv[i+1])
			elif sys.argv[i] == '-zmn':
				zmn = float(sys.argv[i+1])
			elif sys.argv[i] == '-zmx':
				zmx = float(sys.argv[i+1])

		i += 1

	InputData = [[4910,520],[4960,480],[5030,460],[5080,440],[5060,480],[5060,500],[5080,560]]

	if mode:
		M = numpy.array(InputData)
		rM = resizeMat(M[:,px], fftNum, OverLap)
		fM = numpy.array(fftMat(rM))
		if outname is not None:
			f=file(outname,'w')
			fw = csv.writer(f,lineterminator='\n')
			for r in fM:
				if zmn is not None:
					ix = numpy.nonzero(r < zmn)[0]
					r[ix] = zmn
				if zmx is not None:
					ix = numpy.nonzero(r > zmx)[0]
					r[ix] = zmx
				fw.writerow(list(r))
			f.close()
	else:
		fM = numpy.array(InputData)
		if zmn is not None:
			for r in fM:
				ix = numpy.nonzero(r < zmn)[0]
				r[ix] = zmn
		if zmx is not None:
			for r in fM:
				ix = numpy.nonzero(r > zmx)[0]
				r[ix] = zmx

	if log_flg:
		fM = log(fM)

	cmn = numpy.min(fM)
	cmx = numpy.max(fM)

	if zmn is not None:
		if log_flg:
			cmn = log(zmn)
		else:
			cmn = zmn
	if zmx is not None:
		if log_flg:
			cmx = log(zmx)
		else:
			cmx = zmx

	if plot_flg:
		fig, ax = pylab.subplots(figsize=(gx, gy))
		heatmap = ax.pcolor(fM.T)
		xlim, ylim = pylab.xlim(), pylab.ylim()
		xlim = (xlim[0],len(fM.T[0]))
		ylim = (ylim[0],len(fM.T))
		ax.set_xlim(xlim)
		ax.set_ylim(ylim)
		heatmap.set_clim((cmn,cmx))
		if graph_flg:
			pylab.show()

		fig.savefig(graphname, dpi=fig_dpi)